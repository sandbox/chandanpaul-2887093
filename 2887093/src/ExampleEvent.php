<?php

namespace Drupal\example_events;

use Symfony\Component\EventDispatcher\Event;


class ExampleEvent extends Event {

  const SUBMIT = 'event.submit';

  protected $referenceID;

  public function __construct($referenceID)
  {
    $this->referenceID = $referenceID;
  }

  public function getReferenceID()
  {
    return $this->referenceID;
  }
  //This is new message from master branch.
  public function myEventDescription() {
    return "This is as an example event";
  }
//This is test message.
//This is second comment from head point to dev branch.

//This is third.

//This is after change head point from dev.
//This is for patch message.
//This is second patch comment.
}
